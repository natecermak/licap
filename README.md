# README #

LiCaP is the Live Calcium Processor. It's meant to be used with a two-photon microscope running PrairieView (Bruker).

It includes two programs. The first is a C program that runs on the computer running Prairieview and transmits 
imaging data from that computer to another computer (this uses Prairieview's tcp connection to run script commands). 
The second program is in LabView 2017 and receives the imaging data and generates feedback signals based on the
signal intensity in various regions-of-interest (ROIs). 

### Who do I talk to? ###

Nate Cermak (cerman07atprotonmaildotcom)
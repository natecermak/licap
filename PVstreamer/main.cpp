/*  PVStreamer (c) Nathan Cermak 2019
    Goals:
        acquires live images from PrairieView
        aligns those images to a template using a non-rigid method
        saves the aligned tiff files
        saves the motion vectors
        saves images to disk

        optionally streams individual frames to a target IP address


*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <winsock2.h>
#include <conio.h>
#include <time.h>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\opencv.hpp>

#define DATA_WAIT_TIMEOUT 5.0




//only works in increments of 10 ms, very approximate.
void delay(int milliseconds) {
    long pause = milliseconds*(CLOCKS_PER_SEC/1000);
    clock_t now = clock();
    clock_t then = clock();
    while( (now-then) < pause )
        now = clock();
}

void error(const char *msg) {
    perror(msg);
    exit(0);
}

// This function assumes the response will be "ACK\r\n(VALUE\r\n)DONE\r\n"
int getServerResponse(SOCKET server, int timeout){
    clock_t tStart = clock();
    char buffer[256];
	int i = 0, n = 0;
	char* done = strstr(buffer, "DONE");
    while (done == NULL){
        n = recv(server, buffer+i, sizeof(buffer), 0);
        if (n < 0)
            error("ERROR reading from socket");
        i += n;
		done = strstr(buffer, "DONE");
		if (clock() - tStart > timeout)
			return -1;
    }
    buffer[i] = '\0';
    printf("%ld Received: %s", clock(), buffer);
	*done = '\0';
	return(atoi(buffer+5));
}

void pvSend(char*buffer, int len, SOCKET server){
    printf("%ld Sent: %.*s", clock(), len, buffer);
    if (send(server, buffer, len, 0) < 0){
        printf("send() failed with error code : %d" , WSAGetLastError());
        error("");
    }
}


int main(int argc, char *argv[]) {
    char socketBuffer[256];

    WSADATA WSAData;
    if (WSAStartup(MAKEWORD(2,2), &WSAData) != 0)
        error("WSAStartup failed!\n");

    // CONNECT TO PRAIRIEVIEW
    SOCKET pvServer;
    SOCKADDR_IN addr;
    if ( (pvServer=socket(AF_INET, SOCK_STREAM, 0)) == SOCKET_ERROR){
        printf("socket() failed initializing PrairieView socket with error code : %d" , WSAGetLastError());
        exit(EXIT_FAILURE);
    }
 	memset((char *) &addr, 0, sizeof(addr));
    addr.sin_addr.s_addr = inet_addr("127.0.0.1");     //address of target
    addr.sin_family = AF_INET;                         // IPV4 socket
    addr.sin_port = htons(1236);                       // port 1236.
	if (connect(pvServer, (SOCKADDR *)&addr, sizeof(addr)) == SOCKET_ERROR)
        error("Failed to connect to PrairieView!\n");
    printf("Connected to PrairieView!\n");

    //// CONNECT TO LABVIEW
    SOCKET lvServer;
    SOCKADDR_IN lvaddr;
    if ( (lvServer=socket(AF_INET, SOCK_STREAM, 0)) == SOCKET_ERROR) {
        printf("socket() failed initializing labview socket with error code : %d" , WSAGetLastError());
        sprintf(socketBuffer, "-x\r\n");
        pvSend(socketBuffer, strlen(socketBuffer), pvServer);
        getServerResponse(pvServer,1000);
        closesocket(pvServer);
        exit(EXIT_FAILURE);
    }

    memset((char *) &lvaddr, 0, sizeof(lvaddr));
    lvaddr.sin_addr.s_addr = inet_addr((argc>1)?argv[1]:"127.0.0.1");
    lvaddr.sin_family = AF_INET;                         // IPV4 socket
    lvaddr.sin_port = htons(8877);                       // port 8877

    if (connect(lvServer, (SOCKADDR *)&lvaddr, sizeof(lvaddr)) != 0){
        printf("connect() failed in connecting to labview with error code : %d" , WSAGetLastError());
        sprintf(socketBuffer, "-x\r\n");
        pvSend(socketBuffer, strlen(socketBuffer), pvServer);
        getServerResponse(pvServer,1000);
        closesocket(pvServer);
        error("");
	}
    printf("Connected to labview!\n");
    getch();

    const int PPL = 512;                            // pixels per line
    const int FRAME_BYTES = PPL*PPL*2;              // square images at 16-bit resolution
    const int DATA_BUFFER_BYTES = 50*FRAME_BYTES;   // keep a buffer of up to 50 frames (1.6 sec);

    clock_t lastDataTime = clock();
    int myPID = getpid();

   	int spp, nCh = 1;
   	int nSamples;
	long int totalSamplesReceived = 0;
	int bytesSent = 0;

	// dataBuffer receives data from PrairieView.
	// It is averaged (based on multisampling factor) and every other line
	// is reversed and then transferred to frameBuffer.
	uint16_t* dataBuffer = (uint16_t*) malloc(DATA_BUFFER_BYTES);
	uint16_t* frameBuffer = (uint16_t*) malloc(FRAME_BYTES);
	memset((void*) dataBuffer, 0, DATA_BUFFER_BYTES);
    memset((void*) frameBuffer, 0, FRAME_BYTES);

    // SEND STARTUP COMMANDS
    sprintf(socketBuffer, "-SamplesPerPixel\r\n");
	pvSend(socketBuffer, strlen(socketBuffer), pvServer);
    spp = getServerResponse(pvServer,1000); // samples per pixel
    printf("Samples per pixel: %d\n", spp);
    spp = 3; //TESTING

	sprintf(socketBuffer, "-StreamRawData%ctrue%c50\r\n",1, 1);
	pvSend(socketBuffer, strlen(socketBuffer), pvServer);
	getServerResponse(pvServer,1000);

	sprintf(socketBuffer, "-LiveScan%con\r\n",1);
	pvSend(socketBuffer, strlen(socketBuffer), pvServer);
    getServerResponse(pvServer,1000);


    // LOOP TO RECEIVE DATA
    clock_t start = clock();
    int ch, p=0, k=0, line=0;
	while(!kbhit()) {
        // request that PrairieView transfer data into frameBuffer
		sprintf(socketBuffer, "-rrd%c%d%c%I64d%c%d\r\n", 1, myPID, 1, (long long) dataBuffer, 1, DATA_BUFFER_BYTES/2);
        pvSend(socketBuffer, strlen(socketBuffer), pvServer);
        // check how many bytes were transferred by PrairieView
    	nSamples = getServerResponse(pvServer,1000);
        totalSamplesReceived += nSamples;
        if (nSamples == 0){
            delay(30);
            continue;
        }

        // if it's been a while since our last datapoint, assume we're starting a new frame.
        if ( (clock() - lastDataTime)/CLOCKS_PER_SEC > DATA_WAIT_TIMEOUT){
            p=0; //0th pixel
            k=0; //0th sample within pixel
        } else { //it has been less than DATA_WAIT_TIMEOUT milliseconds - data must be continuation of prev frame
            lastDataTime = clock();
        }

        // process the received data.
    	for (int i = 0; i < nSamples; i++){  //i indexes data in dataBuffer
            ch = k % nCh;
            line = p / PPL;
            if (line % 2 == 0)
                frameBuffer[p*nCh + ch] += dataBuffer[i];
            else { // account for the fact that every other line is reversed due to the resonant scanner!
                frameBuffer[nCh*((line+1)*PPL - (p%PPL) - 1) + ch] += dataBuffer[i];
            }

            k = (k+1) % (spp*nCh); // k indexes data in a single pixel
            if (k==0){
                p = (p+1) % (FRAME_BYTES/2/nCh); // update pixel index
                if (p==0){// && argc>2) { // we've filled the buffer, send it out. //TESTING
                    bytesSent = send(lvServer, (char*) frameBuffer, FRAME_BYTES, 0); // send the data out over UDP
                    if (bytesSent != FRAME_BYTES){
                        printf("Failed sending with error code : %d" , WSAGetLastError());
                        error("");
                    }
                    memset((void*) frameBuffer, 0, FRAME_BYTES);
                }
            }
    	}
	}
    printf("Received %ld samples in total.\n", totalSamplesReceived);
    double timeElapsed = (clock()-start)/CLOCKS_PER_SEC;
    printf("Time elapsed: %.2f\n", timeElapsed);
    printf("Net samp/sec: %.0f\n", 1.0*totalSamplesReceived/timeElapsed);

	// close connection nicely
	sprintf(socketBuffer, "-StreamRawData%cfalse\r\n", 1);
	pvSend(socketBuffer, strlen(socketBuffer), pvServer);
	getServerResponse(pvServer,1000);
    sprintf(socketBuffer, "-LiveScan%coff\r\n", 1);
	pvSend(socketBuffer, strlen(socketBuffer), pvServer);
	getServerResponse(pvServer,1000);
	sprintf(socketBuffer, "-x\r\n");
    pvSend(socketBuffer, strlen(socketBuffer), pvServer);
    getServerResponse(pvServer,1000);

    closesocket(pvServer);
    closesocket(lvServer);

	free(dataBuffer);
	free(frameBuffer);
    WSACleanup();

    getch();
    return 0;
}
